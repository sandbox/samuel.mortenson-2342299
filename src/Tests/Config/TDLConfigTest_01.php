<?php

/**
 * @file
 * Definition of Drupal\tdl\Tests\Config\TDLConfigTest_01.php.
 */

namespace Drupal\tdl\Tests\Config;
use Drupal\simpletest\WebTestBase;
use Drupal\Core\Url;
use Drupal\Core\Config\FileStorage;

/**
 * Tests that the tdl_practice module includes a simple settings config.
 *
 * @group tdl
 */
class TDLConfigTest_01 extends WebTestBase {

  public static $modules = array('tdl_practice');

  /**
   * Tests that tdl_practice.settings.yml exists and the related admin form works properly
   */
  public function testConfig() {
    // Test the settings initially to make sure the keys are set correctly
    $config = \Drupal::config('tdl_practice.settings')->getRawData();
    $expected = array(
      'message' => $config['message'],
      'maximum' => $config['maximum'],
      'debug' => $config['debug']
    );
    $this->assertEqual($config, $expected, 'tdl_practice.settings.yml contains the expected keys.', 'TDL');
    // Test submitting the form
    $this->drupalLogin($this->root_user);
    $edit = array(
      'message' => 'Hello, earthlings!',
      'maximum' => 9001,
      'debug' => TRUE
    );
    // Get path to configuration form
    $module_path = \Drupal::moduleHandler()->getModule('tdl_practice')->getPath();
    $file_storage = new FileStorage($module_path);
    $info = $file_storage->read('tdl_practice.info');
    // Test to see if a 'configure' path is set
    $this->assertTrue(isset($info['configure']), 'The "configure" path is set in tdl_practice.info.yml.', 'TDL');
    // Submit the configuration form
    $url = new Url($info['configure']);
    $path = $url->toString();
    $this->drupalPostForm($path, $edit, t('Save configuration'));

    // Check to see that the default value for greeting is correct
    $this->drupalGet($path);
    $this->assertFieldByXPath('//input[@value="Hello, earthlings!"]', NULL, 'The message configuration setting can be set correctly.', 'TDL');
    $this->assertFieldByXPath('//input[@value="9001"]', NULL, 'The maximum configuration setting can be set correctly.', 'TDL');
    $this->assertFieldByXPath('//input[@checked="checked"]', NULL, 'The debug configuration setting can be set correctly.', 'TDL');
  }

}
