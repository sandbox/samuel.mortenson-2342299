<?php

/**
 * @file
 * Definition of Drupal\tdl\Tests\TDLTest. This only really has code coverage over the TDLTestController, as the rest of
 *   the module is tied up in batch callbacks that call Simpletest.
 */

namespace Drupal\tdl\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\Core\Config\FileStorage;

/**
 * Tests if the TDL landing pages render right given test results in the database.
 *
 * @group tdl
 */
class TDLTest extends WebTestBase {

  public static $modules = array('tdl');

  protected $config;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->drupalLogin($this->drupalCreateUser(array('administer unit tests')));
    // Get the Page configuration, which is the Group we'll be running tests on
    $module_path = \Drupal::moduleHandler()->getModule('tdl')->getPath();
    $file_storage = new FileStorage($module_path . '/src/Tests/metadata/');
    $this->config =  $file_storage->read('tdl.page.config');
  }

  /**
   * Test that the main TDL landing page (/tdl) has the Clear Results button and renders Group percentages correctly
   */
  public function testMainPage() {
    // Insert a pass message in the database for Page
    \Drupal::database()->insert('tdl_test_results')
      ->fields(array(
        'method' => 'Drupal\tdl\Tests\Page\TDLPageTest_01->fakeTest()',
        'class' => 'Drupal\tdl\Tests\Page\TDLPageTest_01',
        'result' => 'pass',
        'message' => $this->randomGenerator->string(),
      ))
      ->execute();
    $percentage = 1/count($this->config['tests']) * 100;
    // Test expected case
    $this->drupalGet('tdl');
    $this->assertText("($percentage% Complete)", "Page has one of its tests passing, and displays %$percentage");
    // Test to see if Form is displayed
    $this->assertFieldByXPath('//form[@id="tdl-clear-results-form"]', NULL, 'TDLClearResultsForm exists on page.');
  }

  /**
   * Tests that the Group landing page (/tdl/{group}) has the Multiple Test form and renders correctly.
   */
  public function testGroupPage() {
    // Mark Page Lesson 1 as passed
    \Drupal::database()->insert('tdl_test_results')
      ->fields(array(
        'method' => 'Drupal\tdl\Tests\Page\TDLPageTest_01->fakeTest()',
        'class' => 'Drupal\tdl\Tests\Page\TDLPageTest_01',
        'result' => 'pass',
        'message' => $this->randomGenerator->string(),
      ))
      ->execute();
    // Mark Page Lesson 2 as failed
    \Drupal::database()->insert('tdl_test_results')
      ->fields(array(
        'method' => 'Drupal\tdl\Tests\Page\TDLPageTest_02->fakeTest()',
        'class' => 'Drupal\tdl\Tests\Page\TDLPageTest_02',
        'result' => 'fail',
        'message' => $this->randomGenerator->string(),
      ))
      ->execute();
    $this->drupalGet('tdl/' . $this->config['group']['name']);
    // Test that there is at least one failed test and one passed test on the page
    $this->assertFieldByXPath("//div[contains(@class, 'tdl-pass')]", NULL, 'Passing test found on page.');
    $this->assertFieldByXPath("//div[contains(@class, 'tdl-fail')]", NULL, 'Failing test found on page.');
    // Test to make sure the Form shows up
    $this->assertFieldByXPath('//form[@id="tdl-test-group-form"]', NULL, 'TDLTestGroupForm exists on page.');
  }

  /**
   * Tests that the Test landing page (/tdl/{test}) has the Run Test button and renders correctly
   */
  public function testTestPage() {
    // Insert two fake messages for the method
    \Drupal::database()->insert('tdl_test_results')
      ->fields(array(
        'method' => 'Drupal\tdl\Tests\Page\TDLPageTest_01->testPageExists()',
        'class' => 'Drupal\tdl\Tests\Page\TDLPageTest_01',
        'result' => 'pass',
        'message' => 'TRUE is really TRUE',
      ))
      ->execute();
    \Drupal::database()->insert('tdl_test_results')
      ->fields(array(
        'method' => 'Drupal\tdl\Tests\Page\TDLPageTest_01->testPageExists()',
        'class' => 'Drupal\tdl\Tests\Page\TDLPageTest_01',
        'result' => 'fail',
        'message' => 'FALSE is totes FALSE',
      ))
    ->execute();
    $this->drupalGet('tdl/page/Page Lesson 1');
    // Test that there is a failed message and a passed message on the page
    $this->assertFieldByXPath("//li[contains(@class, 'tdl-pass')]", 'TRUE is really TRUE', 'Passing method found on page.');
    $this->assertFieldByXPath("//li[contains(@class, 'tdl-fail')]", 'FALSE is totes FALSE', 'Failing method found on page.');
    // Test to make sure the Test Button appears
    $this->assertFieldByXPath('//form[@id="tdl-test-form"]', NULL, 'TDLTestForm exists on page.');
  }

}
