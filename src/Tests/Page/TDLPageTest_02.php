<?php

/**
 * @file
 * Definition of Drupal\tdl\Tests\Page\TDLPageTest_02.
1 */

namespace Drupal\tdl\Tests\Page;

use Drupal\simpletest\WebTestBase;

/**
 * Tests if the page /tdl_practice exists and works properly.
 *
 * @group tdl
 */
class TDLPageTest_02 extends WebTestBase {

  public $testId;

  public static $modules = array('tdl_practice', 'node');

  protected $user;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser(array('access content'));
  }

  /**
   * Tests that the /tdl_practice/add/{num1}/{num2} route exists, and works as expected
   */
  public function testPage() {
    // Login as an anonymous user
    $this->drupalLogin($this->user);
    // Test expected case
    $number1 = rand(1,10);
    $number2 = rand(1,10);
    $total = $number1+$number2;
    $this->drupalGet("tdl_practice/add/$number1/$number2");
    $this->assertText($total, "/tdl_practice/add/$number1/$number2 contains the number $total", 'TDL');
    $this->assertTitle("$number1+$number2= | Drupal", "/tdl_practice/add/$number1/$number2 contains the title $number1+$number2=", 'TDL');
    // Test custom access checker
    $this->drupalGet("tdl_practice/add/$number1/HelloWorld");
    $this->assertResponse(403, "/tdl_practice/add/$number1/HelloWorld returns a 403", 'TDL');
  }

}
