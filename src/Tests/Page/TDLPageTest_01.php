<?php

/**
 * @file
 * Definition of Drupal\tdl\Tests\Page\TDLPageTest_01.
1 */

namespace Drupal\tdl\Tests\Page;

use Drupal\simpletest\WebTestBase;

/**
 * Tests if the page /tdl_practice exists and works properly.
 *
 * @group tdl
 */
class TDLPageTest_01 extends WebTestBase {

  public $testId;

  public static $modules = array('tdl_practice', 'node');

  /**
   * Tests that the /tdl_practice page exists and contains the text "Hello World"
   */
  public function testPageExists() {
    $this->drupalLogin($this->drupalCreateUser(array('access content')));
    $this->drupalGet('tdl_practice');
    $this->assertText('Hello World', '/tdl_practice contains the text "Hello World"', 'TDL');
  }

}
