<?php

/**
 * @file
 * Definition of Drupal\tdl\Tests\Entity\TDLEntityTest_01.php.
 */

namespace Drupal\tdl\Tests\Entity;
use Drupal\simpletest\KernelTestBase;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;

/**
 * Tests that the tdl_practice_log Entity exists and contains the required fields.
 *
 * @group tdl
 */
class TDLEntityTest_01 extends KernelTestBase {

  public static $modules = array('tdl_practice');

  protected $entity_manager;

  public function setUp() {
    parent::setUp();
    $this->entity_manager = $this->container->get('entity.manager');
  }

  /**
   * Creates a new tdl_practice_log entity and verifies that "message" and "timestamp" contain good data
   */
  public function testEntityExists() {
    // Create a new log entity
    try {
      $entity = $this->entity_manager->getStorage('tdl_practice_log')
        ->create(array(
          'message' => 'Hello, world!',
        ));
    } catch (PluginNotFoundException $e) {
      $this->fail('The tdl_practice_log entity does not exist', 'TDL');
      return;
    }
    // Check that the timestamp is (relatively) accurate
    $this->assertEqual(date('Ymd', $entity->getCreatedTime()), date('Ymd'), 'getCreatedTime returned an accurate date.', 'TDL');
    // Check that the message was set correctly
    $this->assertEqual($entity->getMessage(), 'Hello, world!', 'getMessage returned the expected content.', 'TDL');
  }

}
