<?php

/**
 * @file
 * Definition of Drupal\tdl\Tests\Block\TDLBlockTest_02.
1 */

namespace Drupal\tdl\Tests\Block;

use Drupal\Component\Plugin\Factory;

/**
 * Tests if tdl_practice block has a default configuration, and a form for changing the greeting
 *
 * @group tdl
 */
class TDLBlockTest_02 extends TDLBlockTestBase {

  /**
   * Tests that the tdl_practice_block has the correct default configuration
   */
  public function testBlockConfiguration() {
    $manager = $this->container->get('plugin.manager.block');
    $display_block = $manager->createInstance('tdl_practice_block');
    $configuration = $display_block->getConfiguration();
    $this->assertEqual('Hello', $configuration['greeting'], 'The default configuration for "greeting" is correct.', 'TDL');
  }

  /**
   * Tests that the tdl_practice_block's form works as expected
   */
  public function testBlockForm() {
    // If you've seen The Room, this might look familiar
    $new_greeting = 'I\'m so tired of this world!';

    // Login as an admin user
    $this->drupalLogin($this->root_user);

    // Add the tdl_practice_block
    $default_theme = \Drupal::config('system.theme')->get('default');
    $edit = array(
      'settings[greeting]' => $new_greeting,
      'region' => 'sidebar_first',
    );
    $this->drupalPostForm('admin/structure/block/add/tdl_practice_block/' . $default_theme, $edit, t('Save block'));

    // Check to see that the default value for greeting is correct
    $this->drupalGet('admin/structure/block/manage/tdlpracticeblock');
    $this->assertText($new_greeting, 'The tdl_practice_block configuration form contains the correct greeting value.', 'TDL');

    // Check to see that the block's content has changed
    $this->drupalGet('node');
    $this->assertText($new_greeting, 'The tdl_practice_block contains the correct greeting value.', 'TDL');
  }

}
