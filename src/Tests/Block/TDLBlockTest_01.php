<?php

/**
 * @file
 * Definition of Drupal\tdl\Tests\Block\TDLBlockTest_01.
1 */

namespace Drupal\tdl\Tests\Block;

/**
 * Tests if tdl_practice block exists and works properly.
 *
 * @group tdl
 */
class TDLBlockTest_01 extends TDLBlockTestBase {

  /**
   * Tests that the tdl_practice_block exists and has the right content
   */
  public function testBlockExists() {
    // Login in as admin, who has permissions to place blocks
    $this->drupalLogin($this->root_user);

    // Place the tdl_practice_block
    $this->drupalPlaceBlock('tdl_practice_block', array('id' => 'tdl_practice_block'), 'The tdl_practice_block exists.', 'TDL');

    // Logout so we can test with an anonymous user
    $this->drupalLogout();

    //  Check that the block's markup is working
    $this->drupalGet('node');
    $this->assertText('Hello World', 'The tdl_practice_block contains the text "Hello World".', 'TDL');
  }

}
