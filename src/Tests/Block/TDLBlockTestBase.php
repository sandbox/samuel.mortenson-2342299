<?php

/**
 * @file
 * Definition of Drupal\tdl\Tests\Block\TDLBlockTestBase.
1 */

namespace Drupal\tdl\Tests\Block;

use Drupal\simpletest\WebTestBase;
use Drupal\Core\Config\FileStorage;

/**
 * Contains shared functions for sub-tests to use.
 *
 * @group tdl
 */
class TDLBlockTestBase extends WebTestBase {

  public static $modules = array('block', 'tdl_practice');

  public $testId;

  /**
   * Tests that the tdl_practice module exists, and that block is added as a dependency
   */
  public function testConfiguration() {
    $module_handler = \Drupal::moduleHandler();
    $this->assertTrue($module_handler->moduleExists('tdl_practice'), 'The TDL Practice module was enabled successfully.', 'TDL');

    // This can be done with ModuleHandler::buildModuleDependencies, but this seems faster
    $module_path = \Drupal::moduleHandler()->getModule('tdl_practice')->getPath();
    $file_storage = new FileStorage($module_path);
    $info = $file_storage->read('tdl_practice.info');
    $this->assertTrue(in_array('block', $info['dependencies']), 'Block is marked as a dependency.', 'TDL');
  }

}
