<?php

/**
 * @file
 * Definition of Drupal\tdl\Tests\Event\TDLEventTest_02.php.
 */

namespace Drupal\tdl\Tests\Event;
use Drupal\simpletest\WebTestBase;

/**
 * Tests that the tdl_practice module includes an Event Subscriber.
 *
 * @group tdl
 */
class TDLEventTest_02 extends WebTestBase {

  public static $modules = array('tdl_practice');

  /**
   * Tests that if a user named 'dave' tries to post admin forms, a warning is displayed
   */
  public function testEventSubscriberAndEvent() {
    $message = 'This mission is too important for me to allow you to jeopardize it.';
    // Create dave and give him basic an admin permission
    $user = $this->drupalCreateUser(array('administer blocks', 'administer site configuration'), 'dave');
    $this->drupalLogin($user);
    // Attempt to post an admin form and check that any warning is present
    $this->drupalPostForm('admin/structure/block', array(), 'Save blocks');
    $this->drupalGet('');
    $this->assertFieldByXPath("//div[contains(@class, 'messages--warning')]", NULL, 'Performing a POST request on an admin form warns dave.', 'TDL');
    $this->assertNoRaw($message, 'The event-triggered warning message is not present.', 'TDL');
    // Try to post the maintenance mode form and check that the correct message exists
    $this->drupalPostForm('admin/config/development/maintenance', array(), 'Save configuration');
    $this->drupalGet('');
    $this->assertRaw($message, 'Posting to the Maintenance admin form shows a different warning message.', 'TDL');
  }

}
