<?php

/**
 * @file
 * Definition of Drupal\tdl\Tests\Event\TDLEventTest_01.php.
 */

namespace Drupal\tdl\Tests\Event;
use Drupal\simpletest\WebTestBase;

/**
 * Tests that the tdl_practice module includes an Event Subscriber.
 *
 * @group tdl
 */
class TDLEventTest_01 extends WebTestBase {

  public static $modules = array('tdl_practice');

  /**
   * Tests that if a user named 'dave' tries to post admin forms, a warning is displayed
   */
  public function testEventSubscriber() {
    $xpath = "//div[contains(@class, 'messages--warning')]";
    $path = 'admin/structure/block';
    // Create dave and give him basic an admin permission
    $user = $this->drupalCreateUser(array('administer blocks'), 'dave');
    $this->drupalLogin($user);
    // Get an admin form and assert that the warning is not present
    $this->drupalGet($path);
    $this->assertNoFieldByXPath($xpath, NULL, 'Performing a GET on an admin form does not warn dave.', 'TDL');
    // Attempt to post an admin form and check that the warning is present
    // @todo File a bug report, drupalPostForm wasn't able to follow the redirect to a relative path
    $this->drupalPostForm($path, array(), 'Save blocks');
    $this->drupalGet('');
    $this->assertFieldByXPath($xpath, NULL, 'Performing a POST request on an admin form warns and redirects dave.', 'TDL');
  }

}
