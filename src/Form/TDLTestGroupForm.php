<?php

/**
 * @file
 * Contains \Drupal\tdl\Form\TDLTestGroupForm.
 */

namespace Drupal\tdl\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\FileStorage;

/**
 * Provides a button for running arbitrary tests.
 */
class TDLTestGroupForm extends FormBase {

  /**
   * Gets the metadata for the target group from file
   *
   * @param string $group The machine name for the target group
   * @return array|bool A config array, or FALSE if there were any errors
   */
  protected function getGroupConfig($group) {
    $module_path = \Drupal::moduleHandler()->getModule('tdl')->getPath();
    $file_storage = new FileStorage($module_path . '/src/Tests/metadata/');
    $config = $file_storage->read('tdl.' . strtolower($group) . '.config');
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tdl_test_group_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $group = $build_info['args'][0];
    $config = $this->getGroupConfig($group);
    $options = array();
    foreach ($config['tests'] as $name=>$info) {
      $options[$name] = $name;
    }
    $display_name = $config['group']['display_name'];
    $form['tests'] = array(
      '#title' => "Run multiple $display_name tests",
      '#type' => 'checkboxes',
      '#options' => $options,
      '#required' => TRUE
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Run selected tests'
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Get the group from the route args
    $build_info = $form_state->getBuildInfo();
    $group = $build_info['args'][0];
    $config = $this->getGroupConfig($group);
    // Foreach test that was queued, add the appropriate test class
    $tests = array();
    foreach ($values['tests'] as $value) {
      if ($value) {
        $tests[] = $config['tests'][$value]['class'];
      }
    }
    // Start a batch job to process the tests
    $batch = array(
      'operations' => array(
        array('_tdl_batch_operation', array($tests)),
      ),
      'finished' => '_tdl_batch_finished',
      'init_message' => count($tests) . " tests queued to be run."
    );
    batch_set($batch);
    return $form;
  }

}
