<?php

/**
 * @file
 * Contains \Drupal\tdl\Form\TDLClearResultsForm.
 */

namespace Drupal\tdl\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\FileStorage;

/**
 * Provides a button for removing all test data.
 */
class TDLClearResultsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tdl_clear_results_form';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Clear all test results'
    );
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Simply truncate the test result table.
    $success = db_truncate('tdl_test_results')
      ->execute();
    drupal_set_message('Test results cleared.');
  }

}
