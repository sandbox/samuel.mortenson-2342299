<?php

/**
 * @file
 * Contains \Drupal\tdl\Form\TDLSkeletonForm.
 */

namespace Drupal\tdl\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\FileStorage;

/**
 * Provides a button for downloading test skeletons.
 */
class TDLSkeletonForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tdl_skeleton_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Download Skeleton',
      '#attributes' => array('class' => array('tdl-skeleton-button'))
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the Test from the route arguments
    $build_info = $form_state->getBuildInfo();
    $group = $build_info['args'][0];
    $test = $build_info['args'][1];
    // Swap out windows-style backslashes, if present
    $form_state->setRedirect('tdl.download', array('group' => $group, 'test' => $test));
  }

}
