<?php

/**
 * @file
 * Contains \Drupal\tdl\Form\TDLTestForm.
 */

namespace Drupal\tdl\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\FileStorage;

/**
 * Provides a button for running arbitrary tests.
 */
class TDLTestForm extends FormBase {

  /**
   * Gets the metadata for the target group from file
   *
   * @param string $group The machine name for the target group
   * @return array|bool A config array, or FALSE if there were any errors
   */
  protected function getGroupConfig($group) {
    $module_path = \Drupal::moduleHandler()->getModule('tdl')->getPath();
    $file_storage = new FileStorage($module_path . '/src/Tests/metadata/');
    $config = $file_storage->read('tdl.' . strtolower($group) . '.config');
    return $config;
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tdl_test_form';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Run Test'
    );
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the Group and Test from the route arguments
    $build_info = $form_state->getBuildInfo();
    $group = $build_info['args'][0];
    $test = $build_info['args'][1];
    $config = $this->getGroupConfig($group);
    $test_config = $config['tests'][$test];
    // Start a batch job
    $batch = array(
      'operations' => array(
        array('_tdl_batch_operation', array(array($test_config['class']))),
      ),
      'finished' => '_tdl_batch_finished',
      'init_message' => "1 test queued to be run."
    );
    batch_set($batch);
    return $form;
  }

}
