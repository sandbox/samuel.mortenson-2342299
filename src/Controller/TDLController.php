<?php

/**
 * @file
 * Contains \Drupal\tdl\Controller\TDLController.
 */

namespace Drupal\tdl\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\FileStorage;
use Drupal\Block\BlockPluginBag;
use Drupal\Component\Plugin\PluginManager;
use GuzzleHttp\Client;
use Drupal\Core\Url;



/**
 * Used during development to debug large blocks of test code, consider removing.
 */
class TDLController extends ControllerBase {

  public function debug() {

  }

}
