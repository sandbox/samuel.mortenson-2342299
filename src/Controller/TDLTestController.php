<?php

/**
 * @file
 * Contains \Drupal\tdl\Controller\TDLTestController.
 */

namespace Drupal\tdl\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\FileStorage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;

/**
 * Provides route responses for tdl.module.
 */
class TDLTestController extends ControllerBase {

  protected $config;

  protected $form_builder;

  protected $database;

  /**
   * Constructs a new TDLTestController object
   *
   * @param ModuleHandlerInterface $module_handler A module handler used to get the TDL path
   * @param FormBuilderInterface $form_builder A form builder used to get related TDL forms
   * @param Connection $database A database connection used for fetching tdl test results
   */
  public function __construct(ModuleHandlerInterface $module_handler, FormBuilderInterface $form_builder, Connection $database) {
    // Initialize the services we'll need
    $this->form_builder = $form_builder;
    $this->database = $database;
    // Load all configs into memory
    $module_path = $module_handler->getModule('tdl')->getPath();
    $file_storage = new FileStorage($module_path . '/src/Tests/metadata/');
    $config =  $file_storage->readMultiple($file_storage->listAll('tdl.'));
    $this->config = array();
    foreach ($config as $current) {
      $this->config[$current['group']['name']] = $current;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('form_builder'),
      $container->get('database')
    );
  }

  /**
   * Checks to see if there are any fail or pass messages in the database. Note that this does not account for test runs
   *  that broke unexpectedly, from seg faults or other unpredictable events.
   *
   * @param string $base_class The class that started the test run, as opposed to the class where $method originated
   * @param string $method A complete or incomplete method name, in the format "$class_name->$method_name()"
   * @return string 'pass', 'fail', or 'unknown' given the messages in the database
   */
  protected function checkResult($base_class, $method) {
    // Check for a single fail message
    $fail = $this->database->select('tdl_test_results')
      ->fields('tdl_test_results')
      ->condition('class', $base_class)
      ->condition('method', db_like($method) . '%', 'LIKE')
      ->condition('result', 'fail')
      ->execute()
      ->fetchAssoc();
    // Check for a single pass message
    $pass = $this->database->select('tdl_test_results')
      ->fields('tdl_test_results')
      ->condition('class', $base_class)
      ->condition('method', db_like($method) . '%', 'LIKE')
      ->condition('result', 'pass')
      ->execute()
      ->fetchAssoc();
    // Any failed message means the test failed
    if ($fail) {
      $result = 'fail';
    }
    // If there's on pass message and no fails, assume the test passed
    elseif ($pass) {
      $result = 'pass';
    }
    // There's nothing that passed/failed in the DB, mark as unknown
    else {
      $result = 'unknown';
    }
    return $result;
  }

  /**
   * Checks a class and its parent's results. Not that parents of parents are not currently checked, but will be.
   *
   * @param string $group The machine name for the target group
   * @param string $base_class The class that started the test run
   * @return string 'pass', 'fail', or 'unknown' given the results from each related class
   */
  protected function checkBaseClassResult($group, $base_class) {
    $config = $this->config[$group];
    // If there are parents, put them above the child class
    if (isset($config['classes'][$base_class]['parents'])) {
      $classes = $config['classes'][$base_class]['parents'];
    }
    else {
      $classes = array();
    }
    // Append the child class
    $classes[] = $base_class;
    $results = array();
    // Check results of each test class
    foreach ($classes as $class) {
      $results[] = $this->checkResult($base_class, $class);
    }
    // Set $result based on the related class results
    if (in_array('unknown', $results)) {
      $result = 'unknown';
    }
    elseif (in_array('fail', $results)) {
      $result = 'fail';
    }
    elseif (in_array('pass', $results)) {
      $result = 'pass';
    }
    // This case is only possible if $classes is empty
    else {
      $result = 'unknown';
    }
    return $result;
  }

  /**
   * Checks the percentage of passing test classes for this group
   *
   * @param string $group The machine name for the target group
   * @return int The percentage of passing test classes for this group
   */
  protected function checkGroupPassPercentage($group) {
    $config = $this->config[$group];
    $pass = 0;
    $unknown = 0;
    // Count the number of passed tests in this group
    foreach ($config['tests'] as $info) {
      $result = $this->checkBaseClassResult($group, $info['class']);
      if ($result == 'pass') {
        ++$pass;
      }
      elseif ($result == 'unknown') {
        ++$unknown;
      }
    }
    // Nothing has been run on any of the tests in this group, return FALSE
    if ($unknown == count($config['tests'])) {
      return FALSE;
    }
    // Calculate the percentage and cast to a string, which is needed for Twig processing later
    $percentage = $pass/count($config['tests']) * 100;
    return (string) $percentage;
  }

  /**
   * Assembles an array of requirements based on a class.
   *
   * @param string $group The machine name for the target group
   * @param string $class The base class that's being checked
   * @return array The requirements associated with this class
   */
  protected function getBaseClassRequirements($group, $class) {
    $config = $this->config[$group];
    $requirements = array();
    // If there are parents, put them above the child class
    if (isset($config['classes'][$class]['parents'])) {
      $classes = $config['classes'][$class]['parents'];
    }
    else {
      $classes = array();
    }
    // Append the child class
    $classes[] = $class;
    // Build the requirements for each test class and method
    foreach ($classes as $class) {
      $methods = $config['classes'][$class]['methods'];
      foreach ($methods as $method) {
        $requirements = array_merge($requirements, $method['requirements']);
      }
    }
    return $requirements;
  }

  /**
   * Page callback for /tdl
   *
   * @return array The assembled page array
   */
  public function buildGroups() {
    $groups = array();
    $content = array();
    // Gather basic information about each test group
    foreach ($this->config as $name=>$config) {
      $groups[] = array(
        'title' => $config['group']['display_name'],
        'name' => $name,
        'result' => $this->checkGroupPassPercentage($name),
        'description' => $config['group']['description'],
      );
    }
    $content['groups-content'] = array(
      '#theme' => 'tdl_main_page',
      '#groups' => $groups,
    );
    // This form will truncate the tdl_test_results table when clicked
    $content['clear-button'] = $this->form_builder->getForm('Drupal\tdl\Form\TDLClearResultsForm');
    $page = array(
      '#title' => 'Test Groups',
      'content' => $content,
      '#attached' => array(
        'library' => array(
          'tdl/tdl'
        )
      )
    );
    return $page;
  }

  /**
   * Page callback for /tdl/{group}
   *
   * @param string $group The machine name for the target group
   * @return array The assembled page array
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If the config isn't found, throw a 404
   */
  public function buildGroup($group) {
    // Config isn't set, assume the group doesn't exist and throw a 404
    if (!isset($this->config[$group])) {
      throw new NotFoundHttpException;
    }
    $config = $this->config[$group];
    $content = array();
    $tests = array();
    // Form each test and test result into an array recognized by tdl_group_page
    foreach ($config['tests'] as $name=>$info) {
      $tests[] = array(
        'title' => $name,
        'description' => $info['description'],
        'result' => $this->checkBaseClassResult($group, $info['class']),
        'requirements' => $this->getBaseClassRequirements($group, $info['class']),
      );
    }
    $content['group-content'] = array(
      '#theme' => 'tdl_group_page',
      '#title' => $group,
      '#description' => $config['group']['description'],
      '#tests' => $tests,
    );
    // This form will let you run multiple tests at once
    $content['form'] = $this->form_builder->getForm('Drupal\tdl\Form\TDLTestGroupForm', $group);
    $page = array(
      '#title' => $config['group']['display_name'],
      'content' => $content,
      '#attached' => array(
        'library' => array(
          'tdl/tdl'
        )
      )
    );
    return $page;
  }

  /**
   * Page callback for /tdl/{group}/{test}
   *
   * @param string $group The machine name for the target group
   * @param string $test The name of the target test
   * @return array The assembled page array
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If the test or group isn't found, throw a 404
   */
  public function buildTest($group, $test) {
    // The group or test isn't found, throw a 404
    if (!isset($this->config[$group]) || !isset($this->config[$group]['tests'][$test])) {
      throw new NotFoundHttpException();
    }
    $config = $this->config[$group];
    $test_config = $config['tests'][$test];
    $content = array();
    $classes = array(
      $test_config['class'] => array(),
    );
    // Add all the parent classes
    if (isset($config['classes'][$test_config['class']]['parents'])) {
      foreach ($config['classes'][$test_config['class']]['parents'] as $parent) {
        $classes[$parent] = array();
      }
    }
    // Build variables for each related test class
    foreach ($classes as $class_name=>$class) {
      $methods = array();
      // Pull in class methods and recent results
      foreach ($config['classes'][$class_name]['methods'] as $method_name=>$method) {
        $messages = array();
        // Get all test messages for the current method
        $query = $this->database->select('tdl_test_results')
          ->fields('tdl_test_results', array('result', 'message'))
          ->condition('class', $test_config['class'])
          ->condition('method', "$class_name->$method_name()")
          ->execute();
        while ($row = $query->fetchAssoc()) {
          // Add this row to the method's message list
          $messages[] = array(
            'result' => $row['result'],
            'message' => $row['message'],
          );
        }
        $methods[$method_name] = array(
          'description' => $method['description'],
          'requirements' => $method['requirements'],
          'messages' => $messages,
          // Fetch the result for this method
          'result' => $this->checkResult($test_config['class'], "$class_name->$method_name()"),
        );
      }
      $classes[$class_name] = array(
        'methods' => $methods,
        'result' => $this->checkResult($test_config['class'], $class_name)
      );
    }
    $content['test-content'] = array(
      '#theme' => 'tdl_test_page',
      '#title' => $test,
      '#description' => $test_config['description'],
      '#classes' => $classes,
      '#requirements' => $this->getBaseClassRequirements($group, $config['tests'][$test]['class']),
    );
    // Hints is an optional field
    if (isset($test_config['hints'])) {
      $content['test-content']['#hints'] = $test_config['hints'];
    }
    // This form lets you run individual tests
    $content['run_test_form'] = $this->form_builder->getForm('Drupal\tdl\Form\TDLTestForm', $group, $test);
    // This form lets you download a module skeleton
    $content['download_test_form'] = $this->form_builder->getForm('Drupal\tdl\Form\TDLSkeletonForm', $group, $test);
    $page = array(
      '#title' => $test,
      'content' => $content,
      '#attached' => array(
        'library' => array(
          'tdl/tdl'
        )
      )
    );
    return $page;
  }

}
