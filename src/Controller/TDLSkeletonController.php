<?php

/**
 * @file
 * Contains \Drupal\tdl\Controller\TDLSkeletonController.
 */

namespace Drupal\tdl\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Archiver\ArchiveTar;
use Drupal\system\FileDownloadController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Allows skeletons to be downloaded and compressed on the fly.
 */
class TDLSkeletonController extends ControllerBase {

  public function downloadSkeleton($group, $test) {
    // I want to avoid spaces in directory/filenames, so spaces are replaced with underscores
    $skeleton = str_replace(' ', '_', $test);
    // Get module path
    $module_path = \Drupal::moduleHandler()->getModule('tdl')->getPath();
    $path = $module_path . '/src/Tests/skeletons/' . $skeleton;
    // Remove old temporary archive
    file_unmanaged_delete(file_directory_temp() . '/' . $skeleton . '.tar.gz');
    // Attempt to construct new archive
    try {
      $archiver = new ArchiveTar(file_directory_temp() . '/' . $skeleton . '.tar.gz', 'gz');
      $archiver->addModify(array($path), 'tdl_practice', $path);
    }
    catch (\Exception $e) {
      // An error occurred, redirect the user back to the test landing page
      drupal_set_message('Error when creating archive: ' . $e->getMessage(), 'error');
      return $this->redirect('tdl.test', array('group' => $group, 'test' => $test));
    }
    // Download the archive
    $download_controller = new FileDownloadController();
    $request = new Request(array('file' => $skeleton . '.tar.gz'));
    return $download_controller->download($request, 'temporary');
  }

}
